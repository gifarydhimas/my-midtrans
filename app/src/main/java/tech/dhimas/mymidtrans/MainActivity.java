package tech.dhimas.mymidtrans;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.util.ArrayList;

import static tech.dhimas.mymidtrans.BuildConfig.BASE_URL;
import static tech.dhimas.mymidtrans.BuildConfig.CLIENT_KEY;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

//    private static final String CLIENT_KEY = "";
//    private static final String BASE_URL = "";
    private static final String TAG = MainActivity.class.getSimpleName();

    private Button payButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SdkUIFlowBuilder.init()
                .setClientKey(CLIENT_KEY)
                .setContext(this)
                .setTransactionFinishedCallback(new TransactionFinishedCallback() {
                    @Override
                    public void onTransactionFinished(TransactionResult result) {
                        // Handle finished transaction here.
                    }
                }) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(BASE_URL)
                .enableLog(true)
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // set theme. it will replace theme on snap theme on MAP ( optional)
                .buildSDK();
        payButton = findViewById(R.id.btn_pay);
        payButton.setOnClickListener(this);
    }

    private void processPayment() {

        TransactionRequest transactionRequest = new TransactionRequest("COME_TEST_PAYMENT_" + System.currentTimeMillis(), 123500);
        ItemDetails itemDetails1 = new ItemDetails("ITEM_EVENT_1", 123500, 1, "Paket 1");

        // Create array list and add above item details in it and then set it to transaction request.
        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        itemDetailsList.add(itemDetails1);

        // Set item details into the transaction request.
        transactionRequest.setItemDetails(itemDetailsList);


        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName("COME");
        userDetail.setEmail("hello@comeapp.id");
        userDetail.setPhoneNumber("+6288888888888");
        userDetail.setUserId("0");

        // Initiate address list
        ArrayList<UserAddress> userAddresses = new ArrayList<>();


        // if shipping address is same billing address
        // you can user type Constants.ADDRESS_TYPE_BOTH
        // NOTE: if you use this, skip initiate shipping and billing address above
        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("Malang");
        userAddress.setCity("Malang");
        userAddress.setCountry("IDN");
        userAddress.setZipcode("65145");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        // Set user address to user detail object
        userDetail.setUserAddresses(userAddresses);

        // Save the user detail. It will skip the user detail screen
        LocalDataHandler.saveObject("user_details", userDetail);

        transactionRequest.setCustomField1("<user token>");
        transactionRequest.setCustomField2("<event_id>");
        transactionRequest.setCustomField3("<voucher>");

        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);

        MidtransSDK.getInstance()
                .startPaymentUiFlow(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_pay) {
            Log.d(TAG, "clicked");
            processPayment();
        }
    }
}
